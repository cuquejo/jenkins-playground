# jenkins-playground
Customized jenkins with docker pipeline (Docker-workflow) and blueocean plugin

### Build this customized jenkins image
```
make build
```

### Start Jenkins service
```
make run
```
You will be able to access it on: http://localhost:8080/

### How to unlock (First run only)
```
make unlock
```

### If you need to check and update the [plugins files](docker/plugins.txt) with latest versions:
```
make plugins-update
```

### Delete all build images and docker containers related to this project
```
make delete-jenkins
```
