TAG         := 2.349-jdk11
NAME        := jenkins-dind-blueocean
DHUB_USER   := leonardocuquejo
CI_REGISTRY := registry-1.docker.io
PWD          = $(shell pwd)
PORT         = 8080

build:
	docker build -t ${DHUB_USER}/${NAME}:${TAG} --build-arg jenTAG=${TAG} -f docker/Dockerfile docker

run:
	docker run --name ${NAME} \
	 --restart=on-failure --detach \
	 -v ${PWD}/jenkins:/var/jenkins_home \
	 -v /var/run/docker.sock:/var/run/docker.sock \
	 --publish ${PORT}:8080 --publish 50000:50000 \
	  ${DHUB_USER}/${NAME}:${TAG}

unlock:
	echo "The initialAdminPassword is:"
	cat ${PWD}/jenkins/secrets/initialAdminPassword

scan:
	docker scan --accept-license --severity medium ${DHUB_USER}/${NAME}:${TAG}

# Publish this image to docker hub
publish: build
	docker tag ${DHUB_USER}/${NAME}:${TAG} ${CI_REGISTRY}/${DHUB_USER}/${NAME}:${TAG}
	docker push ${CI_REGISTRY}/${DHUB_USER}/${NAME}:${TAG}

plugins-update:
	docker exec -it ${NAME} bash -c "jenkins-plugin-cli -f /usr/share/jenkins/ref/plugins.txt --available-updates --output txt" > plugins2.txt
	mv plugins2.txt /tmp/plugins.txt

delete-jenkins:
	docker rm -f jenkins-dind-blueocean
	docker rmi -f ${DHUB_USER}/${NAME}:${TAG}
